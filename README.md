# Weather API

API that allows user to sign up and authenticate to be able to check the weather in an user selected address.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Running the stack

#### Prerequisites

Before running it you should prepare the environment variables.

Create a copy of services.envar.dist named services.envar and set the `GEOCODING_API_KEY` to a Google Geocoding API valid key and `WEATHER_API_KEY` to an Open Weather Api valid Key.

To get a redis server, a mongo db and the api running you should run at the project root folder:

```
sudo bash run-docker.sh
```

This will set 3 docker containers:
 - Redis container at `localhost:6379`
 - MongoDB at `localhost:27017-27019` with a database named `weather_database`
 - Weather API at `localhost:5812`
 
After this you will be set up to develop or test the app.

### Testing the endpoints

After running the stack you should visit `localhost:5812/api-docs` to see a swagger interface that will let you test the endpoints.

### Endpoints

`localhost:5812/user/create`:

This endpoint will create a user in the weather_database users collection saving its email, name, hashed password and the salt used. It will also add a key value pair (key: token, value: email) to Redis that will be used to authenticate the user in further endpoints.

`localhost:5812/user/authenticate`:

This endpoint expects an email and a password, finds the user in the database and if the password matches it adds the key value pair to Redis.

`localhost:5812/user/set-schedule`:

This endpoint expects a token that will be searched in Redis to authenticate the user, if the token matches it will add the user to the request then find and update, if the user exists, the schedule for that given user in the users collection. The endpoint expects a schedule that is an array of strings containing interval of hours, and a timezone that will be used to convert this hours to UTC.

`localhost:5812/address/validate`:

This endpoint uses the same authentication method mentioned before, it accepts an address that will validate against the Google Geocoding Api Service, which will return if it's a valid address and also its location. The response will also be cached in Redis for 10 hours, and if it's queried again it will retrieve the result from Redis.

`localhost:5812/address/check-weather`

This endpoint will validate and locate and address, if possible through Redis, and once it haves the address data it will fetch the Open Weather Api Service to obtain the weather data for that address and cache it in Redis, save it in mongo to be able to retrieve it later to the batch process that checks it.

#### Notes: 

All the endpoints have integrated `@hapi/joi` validation implicitly that will verify that all the current api calls have the required parameters and that they fit the requirements, at the Node.js Challenge pdf document it states that there must be an endpoint that validates an address, an endpoint that checks the weather at the current location, and an endpoint that validates an address and checks the weather.

As validating an address and obtaining its coordinates it's the same api call towards Google Geocoding API Service it means that the 2nd and 3rd endpoint proposed at the pdf are the same in this escenario. So that is why there are only two endpoints instead of three.

## Running the tests

To run the test suite run at the project root folder:

```
npm install
npm run test
```

### Notes

TODO: The tests and coverage of the api could certainly be improved by adding more meaningful tests.


## Jobs

This project uses AgendaJS to schedule a job that checks all the previously queried addresses and checks the weather and if it has some kind of precipitation it sends an email(a test email, which will be logged on the console) to the user(s) who queried it.

The users have an endpoint to set an schedule, an interval of hours in which they wish to be notified so if the address has precipitation but the current time does not match the user' schedule it won't send an email.

## Deployment

Assuming that we have an Amazon VPC with an external loadbalancer that will route the requests to an ECS cluster linked to an EC2 instance with a linux ami in which we will deploy our dockericed app. Also assuming that we have an ElastiCache Redis service and a DynamoDB or a MongoDB instance deployed which will be accessed through an internal load balancer.

We should set a continous integration pipeline that will pass the test suite before allowing to continue with the deployment of the service.

After that we could use AWS CodeBuild to automatically build, push the docker image towards an image container registar as AWS ECR. Then use CodeDeploy to configure the task definition and deploy the task in the target ECS cluster.

Although if the code was on github we could investigate github actions to deploy the service into EC2.

If we were to deploy manually the service manually we would tag and push the image to ECR or DockerHub, then we should go to ECS select the cluster, create a new task definition defining the memory limits, the environment variables and the url of where we pushed our docker image in ECR and all the other required parameters by aws.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
