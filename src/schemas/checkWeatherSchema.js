import Joi from '@hapi/joi';

const checkWeatherSchema = Joi.object({
  street: Joi.string().required(),
  streetNumber: Joi.string().required(),
  town: Joi.string().required(),
  postalCode: Joi.string().required(),
  country: Joi.string().required(),
});

export default checkWeatherSchema;
