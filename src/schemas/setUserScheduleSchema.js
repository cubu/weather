import Joi from '@hapi/joi';

const setUserScheduleSchema = Joi.object({
  schedule: Joi.array().items(Joi.string().regex(/([0-9]-[0-9])/)).required(),
  timezone: Joi.string().required(),
});

export default setUserScheduleSchema;
