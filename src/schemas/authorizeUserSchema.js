import Joi from '@hapi/joi';

const createUserSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
});

export default createUserSchema;
