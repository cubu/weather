import { weatherConfig } from 'config';
import request from 'superagent';

class WeatherService {
  constructor() {
    this.config = {
      key: weatherConfig.key,
      url: weatherConfig.url,
    };
  }

  async checkWeather({ lat, lng }) {
    const url = `${this.config.url}?lat=${lat}&lon=${lng}&appid=${this.config.key}`;

    const { body } = await request.get(url);

    return body;
  }

}

export default new WeatherService();
