import { Client, Status } from '@googlemaps/google-maps-services-js';
import { geocodingConfig } from 'config';

class GeocodingService {
  constructor() {
    this.config = {
      key: geocodingConfig.key,
    };
    this.client = null;
  }

  getClient() {
    if (!this.client) {
      this.client = new Client({});
    }

    return this.client;
  }

  async validateAddress({ street, streetNumber, town, postalCode, country }) {
    const fullAddress = `${streetNumber} ${street} ${town}`;
    const components = `postal_code:${postalCode}|country:${country}`;
    let location = {};
    let valid = false;

    const { data } = await this.getClient().geocode({ params: { address: fullAddress, components, key: geocodingConfig.key } });

    if(data.status == Status.OK) {
      location = data.results[0].geometry.location;
      valid = true;
    }

    return { valid, location };
  }

}

export default new GeocodingService();
