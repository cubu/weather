import argon2 from 'argon2';
import { randomBytes } from 'crypto';
import { userModel } from 'models';
import RedisService from './RedisService';

const blAuthorizeUser = async ({ email, password }) => {
  const userRecord = await userModel.findOne({ email });

  if (!userRecord) {
    throw new Error('User not registered');
  }
  const validPassword = await argon2.verify(userRecord.password, password);

  if(validPassword) {
    const token = randomBytes(32).toString('hex');

    await RedisService.setElement({ key: token, value: userRecord.email, ttl: 60 * 60 * 10 });
    
    return { email, token };
  } else {
    throw new Error('Invalid Password');
  }
};

export default blAuthorizeUser;