import { userModel } from 'models';
import moment from 'moment';


const _convertToUtcHours = ({ schedule, timezone }) => {
  return schedule.map(range => {
    const startHour = parseInt(range.split('-')[0]);
    const endHour = parseInt(range.split('-')[1]);
    const startHourUtc = moment().tz(timezone).hour(startHour).utc().hour();
    const endHourUtc = moment().tz(timezone).hour(endHour).utc().hour();
    
    return `${startHourUtc}-${endHourUtc}`;
  });
};


const blSetUserSchedule = async ({ schedule, timezone, currentUser }) => {
  const utcSchedule = _convertToUtcHours({ schedule, timezone });
  const userRecord = await userModel.findOneAndUpdate({ email: currentUser }, { schedule: utcSchedule }, { useFindAndModify: false });

  if (!userRecord) {
    throw new Error('User not registered');
  }

};

export default blSetUserSchedule;