import RedisService from './RedisService';
import WeatherService from './WeatherService';
import { blAddressValidateAndLocate } from 'services';

const blCheckWeather = async ({ street, streetNumber, town, postalCode, country, currentUser }) => {
  const redisKey = `${country}-${town}-${postalCode}-${street}-${streetNumber}`.replace(/\s/g, '');
  let weatherData = {};

  let addressData = await blAddressValidateAndLocate({ street, streetNumber, town, postalCode, country, currentUser });
  
  if(addressData.valid && addressData.location) {
    if(addressData.weather) {
      weatherData = addressData.weather;
    } else {
      weatherData = await WeatherService.checkWeather({ ...addressData.location });
      addressData.weather = weatherData;
      await RedisService.setElement({ key: redisKey, value: addressData, ttl: 60 * 60 * 12});
    }
  }

  return weatherData;
};

export default blCheckWeather;