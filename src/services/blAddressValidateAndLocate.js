import RedisService from './RedisService';
import GeocodingService from './GeocodingService';
import { addressModel } from 'models';

const blAddressValidateAndLocate = async ({ street, streetNumber, town, postalCode, country, currentUser }) => {
  const redisKey = `${country}-${town}-${postalCode}-${street}-${streetNumber}`.replace(/\s/g, '');

  let validatedAddress = JSON.parse(await RedisService.getElement({ key: redisKey }));

  if(!validatedAddress) {
    validatedAddress = await GeocodingService.validateAddress({ street, streetNumber, town, postalCode, country });
    
    await RedisService.setElement({ key: redisKey, value: validatedAddress, ttl: 60 * 60 * 12 });
  }
  const { location } = validatedAddress;

  if(currentUser) {
    await addressModel.findOneAndUpdate({ lat: location.lat, lng: location.lng }, { street, streetNumber, town, postalCode, country, $addToSet: { users: currentUser }}, { upsert: true, setDefaultsOnInsert: true, useFindAndModify: false });
  }

  return validatedAddress;
};

export default blAddressValidateAndLocate;