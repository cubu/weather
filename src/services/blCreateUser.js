import { randomBytes } from 'crypto';
import argon2 from 'argon2';
import { userModel } from 'models';
import RedisService from './RedisService';

const blCreateUser = async ({ name, email, password }) => {
  const salt = randomBytes(32);
  const hashedPassword = await argon2.hash(password, { salt });

  const userRecord = await userModel.create({
    name,
    email,
    salt: salt.toString('hex'),
    password: hashedPassword,
  });

  if (!userRecord) {
    throw new Error('User cannot be created');
  }

  const token = randomBytes(32).toString('hex');

  await RedisService.setElement({ key: token, value: email, ttl: 60 * 60 * 10 });

  return { name, email, token };
};


export default blCreateUser;