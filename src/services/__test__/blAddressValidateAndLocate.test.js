import blAddressValidateAndLocate from '../blAddressValidateAndLocate';
import RedisServiceMock from '../RedisService';
import GeocodingServiceMock from '../GeocodingService';
import addressModelMock from '../../models';

jest.mock('../RedisService');

jest.mock('../../models', () => ({
  addressModel: { findOneAndUpdate: jest.fn().mockReturnValue({ _id: 1111, name: 'pepe', email: 'pepe@pepe.com' }) },
}));

jest.mock('../GeocodingService');

const params = {
  street: 'Calle Bailen',
  streetNumber: '11',
  town: 'Barcelona',
  postalCode: '08080',
  country: 'Spain',
  currentUser: 'pepe@pepe.com',
};

describe('blAddressValidateAndLocate', () => {
  it('Should validate and locate address when has cache', async () => {
    RedisServiceMock.getElement.mockReturnValue('{"valid":true,"location":{"lat":20,"lng":18}}');
    const getElementParams = { key: `${params.country}-${params.town}-${params.postalCode}-${params.street}-${params.streetNumber}`.replace(/\s/g, '') };
    const setElementParams = { value: params.email, ttl: 60 * 60 * 10 };

    await blAddressValidateAndLocate(params);

    expect(RedisServiceMock.getElement).toBeCalledWith(getElementParams);
    expect(GeocodingServiceMock.validateAddress).not.toBeCalled();
    expect(RedisServiceMock.setElement).not.toBeCalled();
    expect(addressModelMock.addressModel.findOneAndUpdate).toBeCalled();
  });

  it('Should validate and locate address when no cache', async () => {
    RedisServiceMock.getElement.mockReturnValue('""');
    GeocodingServiceMock.validateAddress.mockReturnValue({valid: true, location: {lat: 20,lng: 18}});
    const getElementParams = { key: `${params.country}-${params.town}-${params.postalCode}-${params.street}-${params.streetNumber}`.replace(/\s/g, '') };

    await blAddressValidateAndLocate(params);

    expect(RedisServiceMock.getElement).toBeCalledWith(getElementParams);
    expect(GeocodingServiceMock.validateAddress).toBeCalled();
    expect(RedisServiceMock.setElement).toBeCalled();
    expect(addressModelMock.addressModel.findOneAndUpdate).toBeCalled();
  });

  
});
