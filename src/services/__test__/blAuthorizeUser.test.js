import blAuthorizeUser from '../blAuthorizeUser';
import RedisServiceMock from '../RedisService';
import userModelMock from '../../models';

jest.mock('../../models', () => ({
  userModel: { findOne: jest.fn().mockReturnValue({ _id: 1111, name: 'pepe', email: 'pepe@pepe.com' }) },
}));

jest.mock('../RedisService');

const params = {
  email: 'pepe@pepe.com',
  password: 'pepepassword',
};

describe('blAuthorizeUser', () => {
  it('Should authorize user', async () => {
    const setElementParams = { value: params.email, ttl: 60 * 60 * 10 };

    await blAuthorizeUser(params);

    expect(userModelMock.userModel.findOne).toBeCalledWith({ email: params.email });
    expect(RedisServiceMock.setElement).toBeCalledWith(expect.objectContaining(setElementParams));
  });

  
});
