import blCreateUser from '../blCreateUser';
import RedisServiceMock from '../RedisService';
import userModelMock from '../../models';

jest.mock('../../models', () => ({
  userModel: { create: jest.fn().mockReturnValue({ _id: 1111, name: 'pepe', email: 'pepe@pepe.com' }) },
}));

jest.mock('../RedisService');

const params = {
  name: 'Pepe',
  email: 'pepe@pepe.com',
  password: 'pepepassword',
};

describe('blCreateUser', () => {
  it('Should create user', async () => {
    const setElementParams = { value: params.email, ttl: 60 * 60 * 10 };
    
    await blCreateUser(params);

    expect(userModelMock.userModel.create).toBeCalledWith(expect.objectContaining({ name: params.name, email: params.email}));
    expect(RedisServiceMock.setElement).toBeCalledWith(expect.objectContaining(setElementParams));
  });

  
});
