import blCheckWeather from '../blCheckWeather';
import RedisServiceMock from '../RedisService';
import WeatherServiceMock from '../WeatherService';
import blAddressValidateAndLocateMock from '../blAddressValidateAndLocate';

jest.mock('../blAddressValidateAndLocate');

jest.mock('../RedisService');

jest.mock('../WeatherService');

const params = {
  street: 'Calle Bailen',
  streetNumber: '11',
  town: 'Barcelona',
  postalCode: '08080',
  country: 'Spain',
  currentUser: 'pepe@pepe.com',
};

describe('blCheckWeather', () => {
  it('Should check weather when has cache', async () => {
    blAddressValidateAndLocateMock.mockReturnValue({valid: true, location: {lat: 20,lng: 18}});
    await blCheckWeather(params);

    expect(blAddressValidateAndLocateMock).toBeCalledWith(params);
    expect(WeatherServiceMock.checkWeather).toBeCalled();
    expect(RedisServiceMock.setElement).toBeCalled();
  });
});
