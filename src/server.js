import express from 'express';
import bodyParser from 'body-parser';
import { initMongoose, initAgenda, initJobs, apiDocs } from 'loaders';
import { serverConfig } from 'config';
import routes from 'api/routes';

const { port } = serverConfig;
const app = express();

app.use(bodyParser.json());

app.get('/', (req, res, next) => {
  res.send('API is running...');
  next(); 
});
 
app.get('/ping', (req, res, next) => {
  res.send('pong');
  next();
});

apiDocs(app);

app.use('/', routes);

app.use((err, req, res, next) => {
  const { statusCode = 500, message = '' } = err;

  res.status(statusCode).json({ error: message });
  next();
});

const start = async () => {
  try {
    const mongoConnection = await initMongoose();
    
    const agenda = await initAgenda({ mongoConnection });

    await initJobs({ agenda });
    
    app.listen(port, () => {
      console.log(`Running on ${port}`);
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};

export { app, start };
