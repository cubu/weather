import { addressModel, userModel } from 'models';
import { blCheckWeather } from 'services';
import moment from 'moment';
import nodemailer from 'nodemailer';

const _hasPrecipitation = ({ weather }) => weather.rain || weather.snow;

const _rangeOfHours = ({ start, stop }) => {
  const hoursToNotify = [];
  let shouldContinue = true;
  let currentHour = start;

  while(shouldContinue) {

    if(currentHour === 24) {
      currentHour = 1;
    } else {
      currentHour++;
    }
    hoursToNotify.push(currentHour);

    if(currentHour === stop) {
      shouldContinue = false;
    }
  }

  return hoursToNotify;
};

const _shouldBeNotified = ({ schedule }) => {
  const currentHour = moment().utc().hour() === 0 ? 24 : moment().utc().hour();
  
  schedule.forEach(s => {
    const startHour = parseInt(s.split('-')[0]);
    const endHour = parseInt(s.split('-')[1]) === 0 ? 24 : parseInt(s.split('-')[1]);
  
    const hoursToNotify = _rangeOfHours({ start: startHour, stop: endHour });

    if(hoursToNotify.indexOf(currentHour) >= 0) {
      return true;
    }
  });

  return false;
};

const checkQueriedAddresses = async () => {
  console.log('Checking queried addresses...');
  const addresses = await addressModel.find({});
  const testAccount = await nodemailer.createTestAccount();
  const transporter = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    secure: false,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  });

  for(const address of addresses) {
    const { street, streetNumber, town, postalCode, country, users: addressUsers } = address;
    
    const weather = await blCheckWeather({ street, streetNumber, town, postalCode, country, currentUser: '' });

    if(_hasPrecipitation({ weather })) {
      const usersToNotify = await userModel.find({}).where('email').in(addressUsers);

      for(const userToNotify of usersToNotify) {
        const { schedule } = userToNotify;
        
        if(schedule.length) {
          if(_shouldBeNotified({ schedule })) {
            console.log(`Sending mail to ${userToNotify.email}...`);
            const info = await transporter.sendMail({
              from: '"Weather Man" <christiancuaresma@gmail.com>',
              to: userToNotify.email, 
              subject: 'Precipitation incoming ✔',
              text: 'Find cover',
            });

            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
          }
        }
      }
    }
  }
};

export default checkQueriedAddresses;