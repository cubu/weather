const { AGENDA_DB_COLLECTION, AGENDA_POOL_TIME, AGENDA_CONCURRENCY } = process.env;

const agendaConfig = {
  dbCollection: AGENDA_DB_COLLECTION,
  poolTime: AGENDA_POOL_TIME,
  concurrency: AGENDA_CONCURRENCY,
};

export default agendaConfig;