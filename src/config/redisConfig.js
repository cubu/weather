const { REDIS_HOST, REDIS_PORT } = process.env;

const redisConfig = {
  host: REDIS_HOST,
  port: REDIS_PORT,
};

export default redisConfig;
