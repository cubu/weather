const { MONGO_INITDB_DATABASE, MONGO_INITDB_ROOT_USERNAME, MONGO_INITDB_ROOT_PASSWORD, MONGO_DB_URI } = process.env;

const mongoConfig = {
  user: MONGO_INITDB_ROOT_USERNAME,
  password: MONGO_INITDB_ROOT_PASSWORD,
  host: MONGO_DB_URI,
  database: MONGO_INITDB_DATABASE,
  port: 27017,
};

export default mongoConfig;
