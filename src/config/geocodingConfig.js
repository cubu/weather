const { GEOCODING_API_KEY } = process.env;

const geocodingConfig = {
  key: GEOCODING_API_KEY,
};

export default geocodingConfig;