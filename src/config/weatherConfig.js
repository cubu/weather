const { WEATHER_API_KEY, WEATHER_API_URL } = process.env;

const weatherConfig = {
  key: WEATHER_API_KEY,
  url: WEATHER_API_URL,
};

export default weatherConfig;