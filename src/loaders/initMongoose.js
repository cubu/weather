import mongoose from 'mongoose';
import { mongoConfig } from 'config';

const initMongoose = async () => {
  const connection = await mongoose.connect(mongoConfig.host, { useNewUrlParser: true, useCreateIndex: true });
  
  return connection.connection.db;
};

export default initMongoose;