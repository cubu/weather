import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';

const options = {
  swaggerDefinition: {
    info: {
      description: 'Weather api',
      title: 'Weather',
      version: '0.1.0',
    },
    securityDefinitions: {
      Bearer: {
        type: 'apiKey',
        name: 'Authorization',
        in: 'header',
      },
    },
    basePath: '',
  },
  apis: ['./*/routes/routesDefinition.js', './*/*/routes/routesDefinition.js'],
  swagger: '2.0',
};

const swaggerSpec = swaggerJsDoc(options);

const apiDocs = app => {
  app.get('/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
};

export default apiDocs;
