import Agenda from 'agenda';
import { agendaConfig } from 'config';

const initAgenda = ({ mongoConnection }) => {
  return new Agenda({
    mongo: mongoConnection,
    db: { collection: agendaConfig.dbCollection },
    processEvery: agendaConfig.pooltime,
    maxConcurrency: agendaConfig.concurrency,
  });
};

export default initAgenda;