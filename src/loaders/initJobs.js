
import { agendaConfig } from 'config';
import { checkQueriedAddresses } from 'jobs';

const initJobs = ({ agenda }) => {
  agenda.define(
    'check-queried-addresses',
    { priority: 'high', concurrency: agendaConfig.concurrency },
    checkQueriedAddresses,
  );
  agenda.start();

  agenda.every('60 seconds', 'check-queried-addresses');
};


export default initJobs;