import mongoose from 'mongoose';

const Address = new mongoose.Schema(
  {
    street: {
      type: String,
      required: [true, 'Please enter a street'],
      index: true,
    },
    streetNumber: {
      type: Number,
      index: true,
    },
    postalCode: {
      type: String,
      index: true,
    },
    town: String,
    country: String,
    lat: Number,
    lng: Number,
    users: [String],
  },
  { timestamps: true },
);

Address.index({ lat: 1, lng: 1 }, { unique: true });

export default mongoose.model('Address', Address);
