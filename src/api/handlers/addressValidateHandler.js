import genericHandler from './genericHandler';
import { blAddressValidateAndLocate } from 'services';
import { addressValidateSchema } from 'schemas';

const addressValidateHandler = async (req, res, next) => {
  const { body: { street, streetNumber, town, postalCode, country }, currentUser } = req;

  genericHandler({
    blFunction: blAddressValidateAndLocate,
    res,
    req,
    next,
    schema: addressValidateSchema,
  })({ street, streetNumber, town, postalCode, country, currentUser });
};

export default addressValidateHandler;
