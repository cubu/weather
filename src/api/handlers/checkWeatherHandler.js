import genericHandler from './genericHandler';
import { blCheckWeather } from 'services';
import { checkWeatherSchema } from 'schemas';

const checkWeatherHandler = async (req, res, next) => {
  const { body: { street, streetNumber, town, postalCode, country }, currentUser } = req;

  genericHandler({
    blFunction: blCheckWeather,
    res,
    req,
    next,
    schema: checkWeatherSchema,
  })({ street, streetNumber, town, postalCode, country, currentUser });
};

export default checkWeatherHandler;
