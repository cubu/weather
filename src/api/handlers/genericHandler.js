
const isEmptyObject = obj => Object.entries(obj).length !== 0;

const validate = async ({ body, schema }) => {
  try {
    if (isEmptyObject(body) || isEmptyObject(schema)) {
      await schema.validateAsync(body, { allowUnknown: true });
    }
  } catch (e) {
    const { details } = e;

    throw new Error(`Bad request: ${details[0].message}`, {
      details,
    });
  }
};

const genericHandler = ({
  blFunction,
  res,
  req,
  httpCode = 200,
  next,
  schema = {},
  body = req.body,
}) => {
  return async (...args) => {
    try {
      await validate({ body, schema });
      const result = await blFunction(...args);

      res.status(httpCode).json(result);
      next();
    } catch (e) {
      req._params = req.params;

      next(e);
    }
  };
};

export default genericHandler;
