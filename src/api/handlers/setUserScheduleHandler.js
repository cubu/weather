import genericHandler from './genericHandler';
import { blSetUserSchedule } from 'services';
import { setUserScheduleSchema } from 'schemas';

const setUserScheduleHandler = async (req, res, next) => {
  const { body: { schedule, timezone }, currentUser } = req;

  genericHandler({
    blFunction: blSetUserSchedule,
    res,
    req,
    next,
    schema: setUserScheduleSchema,
  })({ currentUser, timezone, schedule });
};

export default setUserScheduleHandler;
