import express from 'express';
import { createUserHandler, authorizeUserHandler, addressValidateHandler, checkWeatherHandler, setUserScheduleHandler } from 'api/handlers';
import { authorization } from 'api/middlewares';

const router = new express.Router();

router
  .route('/user/create')
  .post(createUserHandler);

router
  .route('/user/authorize')
  .post(authorizeUserHandler);

router
  .route('/user/set-schedule')
  .all(authorization)
  .post(setUserScheduleHandler);

router
  .route('/address/validate')
  .all(authorization)
  .post(addressValidateHandler);

router
  .route('/address/check-weather')
  .all(authorization)
  .post(checkWeatherHandler);

export default router;
