/**
 * @swagger
 * definitions:
 *   Address:
 *     type: object
 *     properties:
 *       street:
 *         type: string
 *       streetNumber:
 *         type: string
 *       town:
 *         type: string
 *       postalCode:
 *         type: string
 *       country:
 *         type: string
 *   CreateUser:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   AuthorizeUser:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   SetUserSchedule:
 *     type: object
 *     properties:
 *       schedule:
 *         type: array
 *         items:
 *           type: string
 *           example: 1-13
 *           description: Interval of hours to be notified in 24 hour format
 *       timezone:
 *         type: string
 *         example: Europe/Madrid
 */

/**
 * @swagger
 * /user/create:
 *   post:
 *     tags:
 *     - "user"
 *     summary: "Create User"
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/CreateUser"
 *     responses:
 *       200:
 *         description: Response ok.
 *       5XX:
 *         description: Unexpected internal error.
 * /user/authorize:
 *   post:
 *     tags:
 *     - "user"
 *     summary: "Authorize User"
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/AuthorizeUser"
 *     responses:
 *       200:
 *         description: Response ok.
 *       5XX:
 *         description: Unexpected internal error.
 * /user/set-schedule:
 *   post:
 *     tags:
 *     - "user"
 *     summary: "Set schedule User"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/SetUserSchedule"
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 * /address/validate:
 *   post:
 *     tags:
 *     - "address"
 *     summary: "Validate address exists via geocoding api and cache to redis"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/Address"
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 * /address/check-weather:
 *   post:
 *     tags:
 *     - "address"
 *     summary: "Validate address exists via geocoding api and cache to redis and checks weather"
 *     security:
 *      - Bearer: []
 *      - Token: []
 *     produces:
 *      - application/json
 *     parameters:
 *     - in: "body"
 *       name: "body"
 *       required: true
 *       schema:
 *         $ref: "#/definitions/Address"
 *     responses:
 *       200:
 *         description: Response ok.
 *       401:
 *         description: Unauthorized
 *       5XX:
 *         description: Unexpected internal error.
 */
